<?php
  mb_internal_encoding("UTF-8");

  $hlaska = '';
  if ($_POST) // V poli _POST něco je, odeslal se formulář
  {
    if (isset($_POST['jmeno']) && $_POST['jmeno'] &&
    isset($_POST['email']) && $_POST['email'] &&
    isset($_POST['zprava']) && $_POST['zprava'] &&
    isset($_POST['rok']) && $_POST['rok'] == date('Y'))
      {
          $hlavicka = 'From:' . $_POST['email'];
          $hlavicka .= "\nMIME-Version: 1.0\n";
          $hlavicka .= "Content-Type: text/html; charset=\"utf-8\"\n";
          $adresa = 'milan.vodak@educanet.cz';
          $predmet = 'Nová zpráva z mailformu';
          $uspech = mb_send_mail($adresa, $predmet, $_POST['zprava'], $hlavicka);
          if ($uspech)
          {
              $hlaska = 'E-mail se podařilo v pořádku odeslat.';
          }
          else
              $hlaska = 'E-mail se bohužel nepodařilo odeslat. Zkontrolujte zadané údaje.';
      }
      else
          $hlaska = 'Formulář jste nevyplnili správně. Prosím oprave chyby';
  }
?>

<!DOCTYPE html>
<html lang="cs" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="{$basePath}/css/base.css">
    <link rel="stylesheet" href="{$basePath}/css/front.css">
    <link rel="stylesheet" href="{$basePath}/css/gallery.css">
    <link rel="stylesheet" href="{$basePath}/css/contact.css">
    <link rel="stylesheet" href="{$basePath}/css/colors.css">
    <link rel="favicon" href="{$basePath}/favicon.ico">
    <title>Kontaktujte mě - MV</title>
  </head>
  <body>
    <?php
      if ($hlaska)
      {
        echo('<p>' . $hlaska . '</p>');
      }
    ?>
    <form method="post">
      <table>
        <tr>
          <td>Jméno:</td>
          <td><input type="text" name="jmeno"></td>
        </tr>
        <tr>
          <td>Vaše emailová adresa:</td>
          <td><input type="email" name="email"></td>
        </tr>
        <TR>
          <td>Jaký je nyní rok:</td>
          <td><input type="number" name="rok"></td>
        </TR>
        <textarea name="zprava">
        </textarea><br />
        <input type="submit" value="Odeslat" />
      </table>
    </form>
    <p><a href="/public">Zpět na hlavní stránku</a></p>
  </body>
</html>
