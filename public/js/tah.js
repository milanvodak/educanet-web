"use strict";

var aktualniHrac = "X";

function priKliknutiNaBunku(infoOUdalosti) {
    // alert("Funguje");

    // Nadefinuj prazdnou promennou elementPolicka
    var elementPolicka;

    // Vloz do elementPolicka bunku <td> z HTML, na kterou se kliknulo
    elementPolicka = infoOUdalosti.target;

    // Vloz do vnitrku bunky (tedy <td>SEM</td>) text "F"
    // elementPolicka.innerText = "F";

    // Pouze pokud je vnitrni obsah elementPolicka prazdny
   if (elementPolicka.innerHTML === "") {
       // prikazy, pokud je podminka splnena
        // Vloz do vnitrku bunky (tedy <td>SEM</td>) text z promenne aktualniHrac
        elementPolicka.innerText = aktualniHrac;

        // Vymen hodnotu v aktualniHrac
        if (aktualniHrac === "X") {
            // prikazy, pokud JE podminka splnena
            aktualniHrac = "O";
        }
        else {
            // prikazy, pokud NENI podminka splnena
            aktualniHrac = "X";
        }
   }
}
