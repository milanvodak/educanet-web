<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get('/', function (Request $request, Response $response, $args){
    return $this->view->render($response, 'index.latte');
})->setName('index');
$app->get('/gallery', function (Request $request, Response $response, $args){
    return $this->view->render($response, 'gallery.latte');
})->setName('gallery');
$app->get('/contact', function (Request $request, Response $response, $args){
    return $this->view->render($response, 'contact.latte');
})->setName('contact');
$app->get('/colors', function (Request $request, Response $response, $args){
    return $this->view->render($response, 'colors.latte');
})->setName('colors');
$app->get('/test', function (Request $request, Response $response, $args){
    return $this->view->render($response, 'test.latte');
})->setName('test');

$app->post('/test-done', function (Request $request, Response $response, $args){
    $data = $request->getParsedBody();
    $body = 0;

    for ($i = 1; $i<=4; $i++){
        if ($data[$i] === 'a') {
            $body = $body + 1;
        }
    }

    $tplVars['body'] = $body;
    return $this->view->render($response, 'test-done.latte', $tplVars);
})->setName('test-done');

$app->get('/games', function (Request $request, Response $response, $args){
    return $this->view->render($response, 'games.latte');
})->setName('games');

?>
